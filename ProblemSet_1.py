def answer_part_1():
    annualSalary = int(input ('Please input your annual salary: '))
    portionSaved = float(input ('The portion you want to save: '))
    totalCost = int(input ('The house price: '))

    portionDownPayment = totalCost * 0.25
    totalSaved = 0
    month = 0
    while (totalSaved<portionDownPayment):
        month += 1
        totalSaved = annualSalary/12*portionSaved + totalSaved * (1 + 0.04 / 12)

    print ('Number of months: ', month)

def answer_part_2():
    annualSalary = int(input('Please input your annual salary: '))
    portionSaved = float(input('The portion you want to save: '))
    totalCost = int(input('The house price: '))
    semiAnnualSalaryRaise = float(input('Input the semi annual raise: '))

    portionDownPayment = totalCost * 0.25
    totalSaved = 0
    month = 0
    while (totalSaved < portionDownPayment):
        print(totalSaved)
        month += 1
        totalSaved = annualSalary / 12 * portionSaved + totalSaved * (1 + 0.04 / 12)
        if month%6 == 0:
            annualSalary *= (1 + semiAnnualSalaryRaise)

    print('Number of months: ', month)

def answer_part_3 (startingSalary):
    # this function takes one parameters so it can be tested automatically
    target = 1000000 * 0.25
    lower = 0
    higher = 10000
    currentSavingRate = 0
    semiAnnualSalaryRaise = 0.07
    step = 0
    totalSaved = 0
    possibel = True
    while ( abs(totalSaved-target)>100):
        step += 1
        annualSalary = startingSalary
        # the acceptable error is 100
        if totalSaved > target:
            # save too much money and need to lower down the limit bound
            higher = currentSavingRate
        else:
            # money saved is too little. Save more money every month
            lower = currentSavingRate
        currentSavingRate = (higher + lower) / 2
        totalSaved = 0
        month = 0
        for i in range (36):
            month +=1
            totalSaved = annualSalary / 12 * (currentSavingRate/10000.0) + totalSaved * (1 + 0.04 / 12)
            if month%6 == 0:
                annualSalary *= (1 + semiAnnualSalaryRaise)
        if step > 17:
            possibel = False
            break
    if (possibel):
        print ('Best saving rate: ', currentSavingRate/10000)
        print ('Steps in bisection search: ', step)
    else:
        print('No way!!!!!!!!!!!!!!!!!')


testCases = [150000, 300000, 10000]

for case in testCases:
    answer_part_3(case)
    print('\n')
